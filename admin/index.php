<!DOCTYPE html>
<html lang="en">
  <head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Dashboard Firefly</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href ="../css/default.css"> 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   </head>

   <body>

   <div class="container-fluid display-table">
   <div class="row display-table-row">
   <!--side menu-->
   <div class="col-md-2 col-sm-1 hidden-xs display-table-cell valign-top" id="side-menu">
   <p><font color="Orange"><h1 class="hidden-sm hidden-xs">Firefly</h1></font></p>
   <ul>
    <li class="link">
    <a href="#">
      <span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span>
      <span class="hidden-sm hidden-xs">Dashboard</span>
      </a>
      </li>

      <li class="link">
      <a href="#collapse-post" data-toggle="collapse" aria-controls="collapse-post">
        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
        <span class="hidden-xs hidden-sm"> Firefly Mobile</span>
        </a>
        <ul class="collapse collapseable" id="collapse-post">

        <li><a href="send.html">Send Notification </a></li>
        <li><a href="about.html"> About us </a></li>
        <li><a href="terms.html"> Terms & Conditions </a></li>
        <li><a href="faq.html"> FAQ </a></li>
         <li><a href="process.html"> Process Log </a></li>
        </ul>
        </li>

        <li class="link">
        <a href="#collapse-user" data-toggle="collapse" aria-controls="collapse-user">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
        <span class="hidden-sm hidden-xs"> User</span>
        </a>
        <ul class="collapse collapseable" id="collapse-user">
        <li><a href="users.html">Users </a></li>
        <li><a href="groups.html"> User Groups</a></li>
        </ul>
        </li>

         <li class="link">
      <a href="#collapse-report" data-toggle="collapse" aria-controls="collapse-report">
        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
       <span class="hidden-sm hidden-xs"> Report </span>
        </a>
        <ul class="collapse collapseable" id="collapse-report">
        <li><a href="report.html">Report </a></li>
        <li><a href="statistical.html"> Statistical Count Report </a></li>
        </ul>
        </li>

         <li class="link">
      <a href="#collapse-sytem" data-toggle="collapse" aria-controls="collapse-sytem">
        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
        <span class="hidden-sm hidden-xs"> System </span>
        </a>
        <ul class="collapse collapseable" id="collapse-sytem">
        <li><a href="stations.html">Stations </a></li>
        <li><a href="banner.html"> Banner </a></li>
        <li><a href="feedback.html"> Feedback </a></li>
        <li><a href="booking.html"> Booking Summary </a></li>
        <li><a href="api.html"> API Audit Log </a></li>
        <li><a href="settings.html"> Settings</a></li>
        <li><a href="audit.html"> Admin Audit Log </a></li>
        <li><a href="housekeeping.html"> Housekeeping </a></li>
        <li><a href="announcements.html"> Announcements </a></li>
        
        </ul>
        </li>
       </div>


   <!-- main content area -->
   <div class="col-md-10 col-sm-11 display-table-cell valign-top">
   <div class="row">
   <header id="nav-header" class="clearfix">
   <div class="col-md-5">
   <nav class="navbar-default pull-left">
   <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#side-menu">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
   </div>

   <div class="col-md-7"></div>
   <ul class="pull-right">
      <li id="welcome" class="hidden-xs"> Welcome to your administration </li>
      <li>
        <a href="#">
        <span class="glyphicon glyphicon-bell" aria-hidden="true"></span>
        </a>
        </li>


        <li>
        <a href="#">
        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
        </a>
        </li>


        <li>
        <a href="../logout.php" class="logout">
        <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
        Log out
        </a>
        </li>
        
        </ul>
        </div>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
        <div class="row">
        <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
        <div class="panel-heading">
        <div class="row">
        <div class="col-xs-3">
        <i class="fa fa-booking fa-5x"></i>
        </div>
        <div class="col-xs-9 text-right">
        <div class="huge">26</div>
        <div>Booking Number</div>
        </div>
        </div>
        </div>
        <a href="#">
        <div class="panel-footer">
        <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">12</div>
                                    <div>Download Application</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                 <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">12</div>
                                    <div>Cancel Booking</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
             <div class="col-lg-3 col-md-6">
             <div class="panel panel-yellow">
             <div class="panel-heading">
             <div class="row">
             <div class="col-xs-3">
             <i class="fa fa-shopping-cart fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
              <div class="huge">124</div>
              <div>New Orders!</div>
              </div>
              </div>
              </div>
                  <a href="#">
                  <div class="panel-footer">
                  <span class="pull-left">View Details</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
                </a>
                </div>
                </div>

   </header>
   <div class="row">
   <footer id="admin-footer" class="clearfix">
   <div class="pull-left"><b>Copyright </b>&copy; 2016</div>
   <div class="pull-right">admin system</div>

   </div>
   </div>
   </div>
   </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/default.js"></script>
  </body>
</html>